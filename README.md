# abenga.com Source Code.

Simple (extremely so) Go web application.

Powers my personal website, [www.abenga.com](http://www.abenga.com).

## Dependencies

* The [gorilla web toolkit](http://www.gorillatoolkit.org). Using
  [context](http://www.gorillatoolkit.org/pkg/context) and
  [mux](http://www.gorillatoolkit.org/pkg/mux).

* [Blackfriday](https://github.com/russross/blackfriday) Markdown processor.

* [alice](https://github.com/justinas/alice), a middleware framework for Go.

* [pq](https://github.com/lib/pq), Go Postgres driver for database/sql.
