module bitbucket.org/abenga/abenga.com

go 1.14

require (
	github.com/fatih/structs v1.1.0
	github.com/gorilla/context v1.1.1
	github.com/gorilla/mux v1.7.4
	github.com/gorilla/sessions v1.2.0
	github.com/jmoiron/sqlx v1.2.0
	github.com/justinas/alice v1.2.0
	github.com/lib/pq v1.6.0
	github.com/russross/blackfriday v1.5.2
	github.com/satori/go.uuid v1.2.0
	golang.org/x/net v0.0.0-20200602114024-627f9648deb9
	golang.org/x/oauth2 v0.0.0-20200107190931-bf48bf16ab8d
	gopkg.in/yaml.v2 v2.3.0
)
