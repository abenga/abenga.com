package models

import (
	"errors"
)

import (
	db "bitbucket.org/abenga/abenga.com/lib/dbconn"
)


func GetPostSeries (where map[string]interface{}, orderByStr, limitStr string, dbConn *db.DBConnection) (*[]PostSeries, error) {
	cols := []string{
		"id", "title", "joined_title", "date_added", "last_edited", "author_id", "abstract_md", "abstract_html", "cover_image_path",
	}
	alias := []string{
		"ID", "Title", "JoinedTitle", "DateAdded", "LastEdited", "AuthorId", "AbstractMD", "AbstractHTML", "CoverImagePath",
	}
	var postSeries []PostSeries
	q, params := db.ConstructSelectQuery("data.post_series", cols, alias, where, orderByStr, limitStr)
	err := dbConn.Conn.Select(&postSeries, q, params...)
	if err != nil {
		return nil, err
	}
	if len(postSeries) == 1 {
		return &postSeries, nil
	}
	return nil, errors.New("Could not retrieve post series from database")
}

//
//func GetPostMetadata (where map[string]interface{}, orderByStr, limitStr string, dbConn *db.DBConnection) (*[]PostSeries, error) {
//	cols := []string{
//		"id", "title", "joined_title", "date_added", "last_edited", "author_id", "abstract_md", "abstract_html", "cover_image_path",
//	}
//	alias := []string{
//		"ID", "Title", "JoinedTitle", "DateAdded", "LastEdited", "AuthorId", "AbstractMD", "AbstractHTML", "CoverImagePath",
//	}
//	var postSeries []PostSeries
//	q, params := db.ConstructSelectQuery("data.post_series", cols, alias, where, orderByStr, limitStr)
//	err := dbConn.Conn.Select(&postSeries, q, params...)
//	if err != nil {
//		return nil, err
//	}
//	if len(postSeries) == 1 {
//		return &postSeries, nil
//	}
//	return nil, errors.New("Could not retrieve post series from database")
//}
//

func (s *PostSeries) NumberOfPosts (dbConn *db.DBConnection) int64 {
	if s.NPosts > 0 {
		return s.NPosts
	}
	var nPosts []int64
	err := dbConn.Conn.Select(&nPosts, `SELECT COUNT(*) AS nPosts FROM data.posts p WHERE p.series_id = $1;`, s.ID)
	if err == nil {
		s.NPosts = nPosts[0]
		return nPosts[0]
	}
	return -1
}


func (s *PostSeries) GetPosts (dbConn *db.DBConnection) (*map[int64]Post, error) {
	where := map[string]interface{}{"series_id": s.ID}
	//postModels, err := GetPosts (where, "", "", dbConn)
	postModels, err := GetPostMetadata(where, "position_in_series", "", dbConn)
	if err != nil {
		return nil, err
	}
	posts := make(map[int64]Post, 0)
	for _, p := range *postModels {
		posts[p.PositionInSeries] = p
	}
	if err != nil {
		return nil, errors.New("Could not get posts")
	}
	s.Posts = &posts
	return &posts, nil
}
