package models

import (
	"errors"
)

import (
	db "bitbucket.org/abenga/abenga.com/lib/dbconn"
	//"github.com/jmoiron/sqlx"
	"log"
)

func GetAuthors (where map[string]interface{}, dbConn *db.DBConnection) (*[]Author, error) {
	cols := []string{
		"id","email",
	}
	alias := []string{
		"ID", "Email",
	}
	var people []Person
	q, params := db.ConstructSelectQuery("core.people", cols, alias, where, "id", "")
	err := dbConn.Conn.Select(&people, q, params...)
	if err != nil {
		log.Println(err)
		return nil, err
	}

	where = make(map[string]interface{}, 0)
	where["id"] = people[0].ID
	cols = []string{
		"id","bio_md", "bio_html",
	}
	alias = []string{
		"ID", "BioMD", "BioHTML",
	}
	var authors []Author
	q, params = db.ConstructSelectQuery("data.authors", cols, alias, where, "id", "")
	err = dbConn.Conn.Select(&authors, q, params...)
	if err != nil {
		log.Println(err)
		return nil, err
	}
	if len(authors) > 0 {
		authors[0].Person = &(people[0])
		return &authors, nil
	}

	return nil, errors.New("Could not retrieve author from database")
}

func (a *Author) GetPerson (dbConn *db.DBConnection) error {
	return nil
}