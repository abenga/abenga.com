package models

import (
	"errors"
	"log"
)

import (
	db "bitbucket.org/abenga/abenga.com/lib/dbconn"

	"github.com/jmoiron/sqlx"
	"fmt"
)


func GetPosts (where map[string]interface{}, orderByStr, limitStr string, dbConn *db.DBConnection) (*[]Post, error) {
	cols := []string{
		"id", "title", "joined_title", "date_added", "last_edited", "year_added",
		"month_added", "day_added", "author_id", "abstract_md", "abstract_html",
		"body_md", "body_html", "series_id", "position_in_series", "references_md",
		"references_html", "cover_image_path", "tags",
	}
	alias := []string{
		"ID", "Title", "JoinedTitle", "DateAdded", "LastEdited", "YearAdded",
		"MonthAdded", "DayAdded", "AuthorId", "AbstractMD", "AbstractHTML",
		"BodyMD", "BodyHTML", "SeriesID", "PositionInSeries", "ReferencesMD",
		"ReferencesHTML", "CoverImagePath", "Tags",
	}
	var posts []Post
	q, params := db.ConstructSelectQuery("data.posts", cols, alias, where, orderByStr, limitStr)
	err := dbConn.Conn.Select(&posts, q, params...)
	if err != nil {
		log.Println(err)
		return nil, err
	}
	if len(posts) > 0 {
		return &posts, nil
	}

	return nil, errors.New("Could not retrieve posts from database")
}


func GetPostMetadata (where map[string]interface{}, orderByStr, limitStr string, dbConn *db.DBConnection) (*[]Post, error) {
	cols := []string{
		"id", "title", "joined_title", "date_added", "last_edited", "year_added", "month_added",
		"day_added", "author_id", "abstract_md", "abstract_html", "series_id", "position_in_series",
	}
	alias := []string{
		"ID", "Title", "JoinedTitle", "DateAdded", "LastEdited", "YearAdded", "MonthAdded",
		"DayAdded", "AuthorId", "AbstractMD", "AbstractHTML", "SeriesID", "PositionInSeries",
	}
	var posts []Post
	q, params := db.ConstructSelectQuery("data.posts", cols, alias, where, orderByStr, limitStr)
	err := dbConn.Conn.Select(&posts, q, params...)
	if err != nil {
		return nil, err
	}
	if len(posts) > 0 {
		return &posts, nil
	}
	return nil, errors.New("Could not retrieve post data from database")
}


func (p *Post) GetPostSeries (dbConn *db.DBConnection) (*PostSeries, error) {
	if p.Series != nil {
		return p.Series, nil
	}
	where := map[string]interface{}{"id": p.SeriesID}
	postSeries, err := GetPostSeries(where, "", "", dbConn)

	if err != nil {
		log.Println(err)
		return nil, err
	}

	if len(*postSeries) == 1 {
		p.Series = &((*postSeries)[0])
		return &((*postSeries)[0]), nil
	} else {
		return nil, errors.New("Incorrect number of series returned.")
	}
}


func (p *Post) Update(tx *sqlx.Tx, changes map[string]interface{}) error {
	where := make(map[string]interface{})
	where["id"] = p.ID
	nUpdated, err := db.Update(tx, "data.posts", changes, where)
	log.Printf("-------------------------------------------------")
	log.Printf("Updated %v rows!!\n", nUpdated)
	log.Printf("-------------------------------------------------")
	if nUpdated == 1 {
		return nil
	} else {
		if err == nil {
			err = errors.New(fmt.Sprintf("updated %v values, but should expected 1", nUpdated))
		}
		return err
	}
}