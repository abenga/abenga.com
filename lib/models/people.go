package models

import (
	"errors"
)

import (
	db "bitbucket.org/abenga/abenga.com/lib/dbconn"
	"github.com/jmoiron/sqlx"
	"time"
	"fmt"
	//"log"
)


func GetLoginSessions (where map[string]interface{}, dbConn *db.DBConnection) (*[]LoginSession, error) {
	cols := []string{
		"id","person_id", "session_id", "time_started", "last_action_time", "ended",
	}
	alias := []string{
		"ID", "PersonID", "SessionID", "TimeStarted", "LastActionTime", "Ended",
	}
	var loginSessions []LoginSession
	q, params := db.ConstructSelectQuery("core.login_sessions", cols, alias, where, "", "")
	err := dbConn.Conn.Select(&loginSessions, q, params...)
	if err != nil {
		return nil, err
	}
	if len(loginSessions) > 0 {
		return &loginSessions, nil
	}

	return nil, errors.New("Could not retrieve login session from database")
}


func GetLoginSession (where map[string]interface{}, dbConn *db.DBConnection) (*LoginSession, error) {
	loginSessions, err := GetLoginSessions(where, dbConn)
	if err == nil && len(*loginSessions) == 1 {
		return &((*loginSessions)[0]), nil
	}
	return nil, err
}


func (l *LoginSession) GetAuthor (tx *sqlx.Tx) (*Author, error) {
	var author Author
	q := "SELECT a.id, a.bio_md, a.bio_html FROM data.authors a WHERE a.ID = $1;"
	err := tx.QueryRow(q, l.PersonID).Scan(&author.ID, &author.BioMD, &author.BioHTML)
	if err != nil {
		return nil, err
	}
	return &author, nil
}


func (p *Person) SaveSessionVars(sessionID string, tx *sqlx.Tx) (int64, error) {
	session := make(map[string]interface{})
	session["person_id"] = p.ID
	session["session_id"] = sessionID
	now := time.Now()
	session["time_started"] = now
	session["last_action_time"] = now
	session["ended"] = false
	if id, err := db.Insert(tx, "core.login_sessions", session, "ID"); err == nil {
		return id.(int64), nil
	} else {
		return -1, err
	}
}


func (l *LoginSession) EndSession(tx *sqlx.Tx) error {
	details := make(map[string]interface{})
	details["ended"] = "t"
	details["time_ended"] = time.Now()
	where := make(map[string]interface{})
	where["id"] = l.ID
	nUpdated, err := db.Update(tx, "core.login_sessions", details, where)
	if err == nil {
		if nUpdated == 1 {
			return nil
		} else {
			return errors.New(fmt.Sprintf("Updated %v rows. Expected 1.", nUpdated))
		}
	}
	return err
}

