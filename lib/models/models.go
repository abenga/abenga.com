package models

import (
	"time"
	"database/sql"
)

import (
	"github.com/lib/pq"
)

type Person struct {
	ID          int     `json:"ID" db_col:"id" db_dtype:"int" db_autofield:"true" valid:"-"`
	Email       string  `json:"Email" db_col:"email" db_dtype:"string" valid:"-"`
	FirstName   string  `json:"Firstname" db_col:"first_name" db_dtype:"string" valid:"-"`
	LastName    string  `json:"LastName" db_col:"last_name" db_dtype:"string" valid:"-"`
	OtherNames  string  `json:"OtherNames" db_col:"other_names" db_dtype:"string" valid:"-"`
}

type Author struct {
	ID          int     `json:"ID" db_col:"id" db_dtype:"int" db_autofield:"true" valid:"-"`
	BioMD       sql.NullString  `json:"BioMD" db_col:"bio_md" db_dtype:"string" valid:"-"`
	BioHTML     sql.NullString  `json:"BioHTML" db_col:"bio_html" db_dtype:"string" valid:"-"`

	Person      *Person
}

type LoginSession struct {
	ID               int64      `json:"id" db_col:"id" db_dtype:"int" db_autofield:"true" valid:"-"`
	PersonID         int64      `json:"person_id" db_col:"person_id" db_dtype:"int" valid:"-"`
	SessionID        string     `json:"session_id" db_col:"session_id" db_dtype:"string" valid:"-"`
	TimeStarted      time.Time  `json:"time_started" db_col:"time_started" db_dtype:"datetime" valid:"-"`
	LastActionTime   time.Time  `json:"last_action_time" db_col:"last_action_time" db_dtype:"datetime" valid:"-"`
	Ended            bool       `json:"ended" db_col:"ended" db_dtype:"boolean" valid:"-"`
	TimeEnded        time.Time  `json:"time_ended" db_col:"time_ended" db_dtype:"datetime" valid:"-"`
}

type PostSeries struct {
	ID               int64     `json:"ID" db_col:"id" db_dtype:"int" db_autofield:"true" valid:"-"`
	Title            string    `json:"Title" db_col:"title" db_dtype:"string" valid:"-"`
	JoinedTitle      string    `json:"JoinedTitle" db_col:"joined_title" db_dtype:"string" valid:"-"`
	AbstractMD       string    `json:"AbstractMD" db_col:"abstract_md" db_dtype:"string" valid:"-"`
	AbstractHTML     string    `json:"AbstractHTML" db_col:"abstract_html" db_dtype:"string" valid:"-"`
	AuthorID         int64     `json:"AuthorID" db_col:"author_id" db_dtype:"string" valid:"-"`
	DateAdded        time.Time `json:"DateAdded" db_col:"date_added" db_dtype:"datetime" valid:"-"`
	LastEdited       time.Time `json:"LastEdited" db_col:"last_edited" db_dtype:"datetime" valid:"-"`
	Tags             []string  `json:"Tags" db_col:"tags" db_dtype:"jsonb" valid:"-"`
	CoverImagePath   string    `json:"CoverImagePath" db_col:"cover_image_path" db_dtype:"string" valid:"-"`

	NPosts           int64
	Posts            *map[int64]Post // keys: positioninseries; values: Posts
}

type Post struct {
	ID               int64            `json:"ID" db_col:"id" db_dtype:"int" db_autofield:"true" valid:"-"`
	Title            string           `json:"Title" db_col:"title" db_dtype:"string" valid:"-"`
	JoinedTitle      string           `json:"JoinedTitle" db_col:"joined_title" db_dtype:"string" valid:"-"`
	DateAdded        time.Time        `json:"DateAdded" db_col:"date_added" db_dtype:"datetime" valid:"-"`
	LastEdited       time.Time        `json:"LastEdited" db_col:"last_edited" db_dtype:"datetime" valid:"-"`
	YearAdded        int64            `json:"YearAdded" db_col:"year_added" db_dtype:"int" db_autofield:"false" valid:"-"`
	MonthAdded       int64            `json:"MonthAdded" db_col:"month_added" db_dtype:"int" db_autofield:"false" valid:"-"`
	DayAdded         int64            `json:"DayAdded" db_col:"day_added" db_dtype:"int" db_autofield:"false" valid:"-"`
	AuthorID         int64            `json:"AuthorID" db_col:"title" db_dtype:"string" valid:"-"`
	Tags             pq.StringArray   `json:"Tags" db_col:"tags" db_dtype:"array[string]" valid:"-"`
	AbstractMD       string           `json:"AbstractMD" db_col:"abstract_md" db_dtype:"string" valid:"-"`
	AbstractHTML     string           `json:"AbstractHTML" db_col:"abstract_html" db_dtype:"string" valid:"-"`
	BodyMD           string           `json:"BodyMD" db_col:"body_md" db_dtype:"string" valid:"-"`
	BodyHTML         string           `json:"BodyHTML" db_col:"body_html" db_dtype:"string" valid:"-"`
	SeriesID         int64            `json:"SeriesID" db_col:"series_id" db_dtype:"string" valid:"-"`
	PositionInSeries int64            `json:"PositionInSeries" db_col:"position_in_series" db_dtype:"int" db_autofield:"false" valid:"-"`
	ReferencesMD     sql.NullString   `json:"ReferencesMD" db_col:"references_md" db_dtype:"string" valid:"-"`
	ReferencesHTML   sql.NullString   `json:"ReferencesHTML" db_col:"references_html" db_dtype:"string" valid:"-"`
	CoverImagePath   sql.NullString   `json:"CoverImagePath" db_col:"cover_image_path" db_dtype:"string" valid:"-"`

	// OtherAttributes
	Series           *PostSeries
}
