package lib

import (
	"errors"
	"crypto/rand"
	"encoding/base64"
	"html/template"
	"os"
)

import (
	"io/ioutil"
	"log"
	"gopkg.in/yaml.v2"
	"net/http"
	"github.com/gorilla/sessions"
)

import (
	"bitbucket.org/abenga/abenga.com/lib/models"
	db "bitbucket.org/abenga/abenga.com/lib/dbconn"
	"strings"
	"fmt"
)


type PageContext struct {
	Author      *models.Author
	Misc        map[string]interface{}
	PostSeries  map[string]template.HTML
	Post        map[string]template.HTML
	LogoutURL   string
	BasePath    string
	Session     *sessions.Session
}

const SESSION_NAME = "abenga.com.session.keys"

type Configuration struct {
	ClientID             string `yaml:"client_id"`
	ClientSecret         string `yaml:"client_secret"`
	LocalClientID        string `yaml:"local_client_id"`
	LocalClientSecret    string `yaml:"local_client_secret"`
	AuthURI              string `yaml:"auth_uri"`
	TokenURI             string `yaml:"token_uri"`
	AuthProviderCertURL  string `yaml:"auth_provider_x509_cert_url"`
	SessionSecret        string `yaml:"session_secret"`
	AuthorEmail          string `yaml:"author_email"`

	DBHost               string `yaml:"db_host"`
	DBUser               string `yaml:"db_user"`
	DBPassword           string `yaml:"db_password"`
	DBName               string `yaml:"db_name"`

	TestDBHost           string `yaml:"test_db_host"`
	TestDBUser           string `yaml:"test_db_user"`
	TestDBPassword       string `yaml:"test_db_password"`
	TestDBName           string `yaml:"test_db_name"`
}

type FlashMessage struct {
	Type string
	Text string
}


func GetDBConn() (*db.DBConnection, error) {
	var cfg Configuration
	cfg.GetConfiguration()
	if conn, err := db.Connect(cfg.DBUser, cfg.DBPassword, cfg.DBHost, cfg.DBName); err == nil {
		return conn, nil
	} else {
		return nil, errors.New("Database connection error.")
	}
}


func (c *Configuration) GetConfiguration() *Configuration {
	goPath := os.Getenv("GOPATH")
	configFilePath := goPath + "/src/bitbucket.org/abenga/abenga.com/config.yaml"
	yamlFile, err := ioutil.ReadFile(configFilePath)
	if err != nil {
		log.Printf("yamlFile.Get err   #%v ", err)
	}
	err = yaml.Unmarshal(yamlFile, c)
	if err != nil {
		log.Fatalf("Unmarshal: %v", err)
	}
	return c
}


func GetOauthClientDetails(c Configuration, r *http.Request) (string, string, string) {
	if r.Host == "www.abenga.com" {
		return c.ClientID, c.ClientSecret, "http://www.abenga.com/author/sign_in_google_callback/"
	} else {
		return c.LocalClientID, c.LocalClientSecret, "http://localhost:8080/author/sign_in_google_callback/"
	}
}


func GetSessionStore() *sessions.CookieStore {
	var cfg Configuration
	cfg.GetConfiguration()
	sessionStore := sessions.NewCookieStore([]byte(cfg.SessionSecret))
	return sessionStore
}


func GenerateRandomBytes(n int) ([]byte, error) {
	b := make([]byte, n)
	if _, err := rand.Read(b); err == nil {
		return b, nil
	} else {
		return nil, err
	}
}


func GenerateRandomString(s int) (string, error) {
	b, err := GenerateRandomBytes(s)
	return base64.URLEncoding.EncodeToString(b), err
}


func CreateOauthRandomString(w http.ResponseWriter, r *http.Request) (string, error) {
	sessionStore := GetSessionStore()
	session, _ := sessionStore.Get(r, "abenga.com")
	oauthString, err := GenerateRandomString(32);
	if err != nil {
		return "", err
	}
	session.Values["oauth_string"] = oauthString
	if err := session.Save(r, w); err == nil {
		return oauthString, nil
	} else {
		return "", err
	}
}


func GetSavedOauthRandomString(w http.ResponseWriter, r *http.Request) (string, error) {
	sessionStore := GetSessionStore()
	session, _ := sessionStore.Get(r, "abenga.com")
	if len(session.Values) > 0 {
		return session.Values["oauth_string"].(string), nil
	} else {
		return "", errors.New("Could not retrieve session id")
	}
}


func GetSessionID(w http.ResponseWriter, r *http.Request) (string, error) {
	sessionStore := GetSessionStore()
	session, _ := sessionStore.Get(r, "abenga.com")
	sessionID, err := GenerateRandomString(32);
	if err != nil {
		return "", err
	}
	session.Values["SessionID"] = sessionID
	if err := session.Save(r, w); err == nil {
		return sessionID, nil
	} else {
		return "", err
	}
}


func GetActiveSession(w http.ResponseWriter, r *http.Request) (string, error) {
	sessionStore := GetSessionStore()
	session, _ := sessionStore.Get(r, SESSION_NAME)
	if len(session.Values) > 0 {
		if session.Values["SessionID"] != nil {
			return string(session.Values["SessionID"].(string)), nil
		}
		return "", errors.New("could not retrieve session id")
	} else {
		return "", errors.New("could not retrieve session id")
	}
}


func ArrayToString(a []string, delim string) string {
    return strings.Trim(strings.Replace(fmt.Sprint(a), " ", delim, -1), "[]")
}