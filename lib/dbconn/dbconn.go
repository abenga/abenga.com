// Package dbconn provides convenient sql generation functions for common
// operations such as inserting rows into an sql database, updating specified
// rows, retrieving rows from the database, etc.
package dbconn

import (
	"database/sql"
	"fmt"
	"strings"
	"reflect"
)

import (
	_ "github.com/lib/pq"
	"github.com/jmoiron/sqlx"
	"github.com/fatih/structs"
	"github.com/satori/go.uuid"
	//"log"
	"log"
)

type DBConfig struct {
	Server   string
	Database string
	User     string
	Password string
}

type DBConnection struct {
	Conn *sqlx.DB
}

const DB_COL_NAME_TAG = "db_col"
const DB_COL_TYPE_TAG = "db_dtype"
const DB_COL_AUTO_FIELD_TAG = "db_autofield"

func Connect(user, password, server, database string) (*DBConnection, error) {
	connStr := fmt.Sprintf("postgres://%s:%s@%s/%s", user, password, server, database)
	db, err := sqlx.Connect("postgres", connStr)
	if err == nil {
		var conn DBConnection
		conn.Conn = db
		return &conn, nil
	} else {
		return nil, err
	}
}

// Execute a query q in the context of transaction tx, with arbitrary
// arguments, and return a pointer to an sql.Rows object.
func Query(tx *sqlx.Tx, q string, args ...interface{}) (*sql.Rows, error) {
	if stmt, err := tx.Prepare(q); err == nil {
		return stmt.Query(args...)
	} else {
		// log.Println("Error preparing query statement:")
		// log.Printf("Query: \"%v\"", q)
		return nil, err
	}
}

// Execute a query q in the context of transaction tx, with arbitrary
// arguments, and return a pointer to an sql.Row object.
func QueryRow(tx *sqlx.Tx, q string, args ...interface{}) (*sql.Row, error) {
	if stmt, err := tx.Prepare(q); err == nil {
		return stmt.QueryRow(args...), nil
	} else {
		// log.Println("Error preparing query statement:")
		// log.Printf("Query: \"%v\"", q)
		// log.Printf("Error: \"%v\"", err)
		return nil, err
	}
}

// Execute an sql statement q (which will not return rows) in the context of
// transaction tx, with arbitrary arguments, and return a pointer to an
// sql.Result object.
func Exec(tx *sqlx.Tx, q string, args ...interface{}) (sql.Result, error) {
	if stmt, err := tx.Prepare(q); err == nil {
		if result, err := stmt.Exec(args...); err == nil {
			return result, nil
		} else {
			return nil, err
		}
	} else {
		// log.Println("Error preparing query statement:")
		// log.Printf("Query: \"%v\"\n", q)
		// log.Printf("Error: \"%v\"\n", err)
		// log.Printf("---------------------------------")
		return nil, err
	}
}

// Insert tries to insert an object into the database, in the context of
// transaction tx.
// If the "returning" argument is provided, the value of that column in the
// inserted row is returned.
func Insert(tx *sqlx.Tx, table string, obj map[string]interface{}, returning string) (interface{}, error) {
	var components []string
	components = append(components, fmt.Sprintf("INSERT INTO %v ( ", table))
	var cols []string
	var placeholders []string
	var params []interface{}
	i := 1
	for k, v := range obj {
		cols = append(cols, k)
		placeholders = append(placeholders, fmt.Sprintf("$%v", i))
		params = append(params, v)
		i = i + 1
	}
	colStr := strings.Join(cols, ", ")
	components = append(components, colStr)
	components = append(components, ") VALUES (")
	valString := strings.Join(placeholders, ", ")
	components = append(components, valString)
	components = append(components, ")")
	if returning != "" {
		components = append(components, fmt.Sprintf(" RETURNING %v", returning))
	}
	q := strings.Join(components, "")
	if returning != "" {
		if rows, err := Query(tx, q, params...); err == nil {
			var r interface{}
			defer rows.Close()
			i := 0
			for rows.Next() {
				err = rows.Scan(&r)
				i += 1
			}
			if err == nil {
				return r, nil
			} else {
				return -1, err
			}
		} else {
			return -1, err
		}
	} else {
		if _, err := Exec(tx, q, params); err == nil {
			return -1, nil
		} else {
			return -1, nil
		}
	}
}

func AddModel(tx *sqlx.Tx, table string, obj interface{}, returning string) (interface{}, error) {
	fullStruct := structs.New(obj).Map()
	st := reflect.TypeOf(obj)
	dataMap := make(map[string]interface{})
	for i := 0; i < st.NumField(); i++ {
		field := st.Field(i)
		colName := field.Tag.Get(DB_COL_NAME_TAG)
		isAutoField := field.Tag.Get(DB_COL_AUTO_FIELD_TAG) // set by db, e.g. autoincrement fields
		dataType := field.Tag.Get(DB_COL_TYPE_TAG)
		if fullStruct[field.Name] != nil && isAutoField == "false" {
			if dataType == "jsonb" {

			} else if dataType == "datetime" {
				//dataMap[colName] = fullStruct[field.Name].(time.Time).Format("2006-01-02 15:04:05")
				dataMap[colName] = fullStruct[field.Name]
			} else if dataType == "string" && fullStruct[field.Name] != "" {
				dataMap[colName] = fullStruct[field.Name]
			} else if dataType == "uuid" {
				//log.Println(colName, fullStruct[field.Name])
				dataMap[colName] = fullStruct[field.Name].(uuid.UUID).String()
			} else {
				if fullStruct[field.Name] != "" {
					dataMap[colName] = fullStruct[field.Name]
				}
			}
		}
	}
	return Insert(tx, "operations.packages", dataMap, returning)
}

// Update tries to update a row in the database, in the context of
// transaction tx.
// It returns the number of rows updated.
func Update(tx *sqlx.Tx, table string, details, where map[string]interface{}) (int64, error) {
	var components []string
	components = append(components, fmt.Sprintf("UPDATE %v SET ", table))

	var placeholders []string
	var params []interface{}
	i := 1
	for k, v := range details {
		placeholders = append(placeholders, fmt.Sprintf("%v = $%v", k, i))
		params = append(params, v)
		i = i + 1
	}
	valString := strings.Join(placeholders, ", ")
	components = append(components, valString)

	components = append(components, "WHERE")

	var whereList []string
	for k, v := range where {
		whereList = append(whereList, fmt.Sprintf("%v = $%v", k, i))
		params = append(params, v)
		i = i + 1
	}
	whereStr := strings.Join(whereList, " AND ")
	components = append(components, whereStr)
	components = append(components, ";")
	q := strings.Join(components, " ")
	log.Println("=========================================================")
	log.Println(q)
	log.Println("=========================================================")
	if result, err := Exec(tx, q, params...); err == nil {
		return result.RowsAffected()
	} else {
		return -1, err
	}
}

func GetTableRows(tx *sqlx.Tx, table string, cols []string, alias []string, where map[string]interface{}) (*sql.Rows, error) {
	q, params := ConstructSelectQuery(table, cols, alias, where, "", "")
	if rows, err := Query(tx, q, params...); err == nil {
		return rows, nil
	} else {
		return nil, err
	}
}

func ConstructSelectQuery(table string, cols, alias []string, where map[string]interface{}, orderByStr, limitStr string) (string, []interface{}) {
	var useAlias = false
	if len(cols) == len(alias) {
		useAlias = true
	}
	var components []string
	components = append(components, "SELECT")
	var colStr string
	if useAlias {
		aliasCols := make([]string, 0)
		for i, col := range cols {
			aliasCols = append(aliasCols, col + " AS " + alias[i])
		}
		colStr = strings.Join(aliasCols, ", ")
	} else {
		colStr = strings.Join(cols, ", ")
	}

	components = append(components, colStr)
	components = append(components, fmt.Sprintf("FROM %v", table))
	var whereList []string
	var params []interface{}
	i := 1
	for k, v := range where {
		whereList = append(whereList, fmt.Sprintf("%v = $%v", k, i))
		params = append(params, v)
		i = i + 1
	}

	//components = append(components, "FROM", table)

	if len(where) > 0 {
		whereStr := strings.Join(whereList, " AND ")
		components = append(components, " WHERE ", whereStr)
	}

	if orderByStr != "" {
		components = append(components, "ORDER BY", orderByStr)
	}
	if limitStr != "" {
		components = append(components, "LIMIT", limitStr)
	}
	components = append(components, ";")
	q := strings.Join(components, " ")
	//log.Println("---------------------------------------------")
	//log.Println(q)
	//log.Println("---------------------------------------------")
	//log.Println(params)
	//log.Println("---------------------------------------------")
	return q, params
}
