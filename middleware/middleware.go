package middleware

import (
	"net/http"
	"log"
	"os"
)

import (
	"github.com/gorilla/context"
	//"github.com/gorilla/sessions"
)

import (
	"bitbucket.org/abenga/abenga.com/lib"
	models "bitbucket.org/abenga/abenga.com/lib/models"
	//"github.com/gorilla/sessions"
)

// Set up the page variables struct. If the request is being made by an
// authenticated author. If this is true, we set the PageContext author field.
func Initialize(next http.Handler) http.Handler {
	handler := func(w http.ResponseWriter, r *http.Request) {
		log.Printf("%v %s", r.Method, r.URL.Path)

		ctx := new(lib.PageContext)
		ctx.Misc = make(map[string]interface{})

		store := lib.GetSessionStore()
		session, _ := store.Get(r, lib.SESSION_NAME)
		ctx.Session = session

		goPath := os.Getenv("GOPATH")
		ctx.BasePath = goPath + "/src/bitbucket.org/abenga/abenga.com/"

		context.Set(r, "PageContext", ctx)
		next.ServeHTTP(w, r)
	}
	return http.HandlerFunc(handler)
}

// If the author has not been authorized, redirect the user to a login page.
func CheckAuth(next http.Handler) http.Handler {
	handler := func(w http.ResponseWriter, r *http.Request) {
		pageContext := context.Get(r, "PageContext").(*lib.PageContext)
		sessionID, _ := lib.GetActiveSession(w, r)

		dbConn, _ := lib.GetDBConn()
		defer dbConn.Conn.Close()

		if sessionID != "" {
			where := map[string]interface{}{"session_id": sessionID}
			loginSession, _ := models.GetLoginSession(where, dbConn)
			if loginSession != nil {
				where = make(map[string]interface{}, 0)
				where["id"] = loginSession.PersonID
				authors, _ := models.GetAuthors(where, dbConn)
				if len(*authors) == 1 {
					pageContext.Author = &((*authors)[0])
					log.Println(pageContext.Author)
				}
			}
		}
		context.Set(r, "PageContext", pageContext)
		if pageContext.Author != nil {
			next.ServeHTTP(w, r)
		} else {
			log.Println("Failed to find author")
			w.Header().Set("Location", "/")
			w.WriteHeader(http.StatusFound)
		}
	}
	return http.HandlerFunc(handler)
}
