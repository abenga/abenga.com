package main

import (
	"log"
	"net/http"
)

import (
	"bitbucket.org/abenga/abenga.com/routes"
)

func main() {
	router := routes.NewRouter()
	http.Handle("/", router)

	log.Println("Listening on http://localhost:8080")
	http.ListenAndServe(":8080", nil)
}
