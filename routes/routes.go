package routes

import (
	"net/http"
)

import (
	"github.com/gorilla/context"
	"github.com/gorilla/mux"
	//"github.com/gorilla/handlers"
	"github.com/justinas/alice"
)

import (
	mw "bitbucket.org/abenga/abenga.com/middleware"
	"bitbucket.org/abenga/abenga.com/handlers"
	author "bitbucket.org/abenga/abenga.com/handlers/author"
)

type Route struct {
    Name     string
    Methods  []string
    Pattern  string
    Handler  http.Handler
}

type Routes []Route

var plainMW = alice.New(context.ClearHandler, mw.Initialize)
var authMW = alice.New(context.ClearHandler, mw.Initialize, mw.CheckAuth)

var routeList = Routes{
	Route{
		"Index",
		[]string{"GET"},
		"/",
		plainMW.ThenFunc(handlers.Index),
	},

	Route{
		"Posts",
		[]string{"GET"},
		"/posts/",
		plainMW.ThenFunc(handlers.Posts),
	},

	Route{
		"PostSeriesHome",
		[]string{"GET"},
		"/postseries/{seriesjtitle}/",
		plainMW.ThenFunc(handlers.PostSeries),
	},

	Route{
		"Post",
		[]string{"GET"},
		"/post/{yearadded}/{monthadded}/{dayadded}/{joinedtitle}/",
		plainMW.ThenFunc(handlers.Post),
	},

	Route{
		"SignIn",
		[]string{"GET"},
		"/author/sign_in/",
		plainMW.ThenFunc(author.SignIn),
	},

	Route{
		"GoogleCallback",
		[]string{"GET"},
		"/author/sign_in_google_callback/",
		plainMW.ThenFunc(author.GoogleCallback),
	},

	Route{
		"AuthorHome",
		[]string{"GET"},
		"/author/",
		authMW.ThenFunc(author.Home),
	},
	//Route{
	//	"NewPost",
	//	[]string{"GET"},
	//	"/author/new_post/",
	//	plainMW.ThenFunc(author.NewPost),
	//},
	//Route{
	//	"NewPostSeries",
	//	[]string{"GET"},
	//	"/author/newpostseries/",
	//	plainMW.ThenFunc(author.NewPostSeries),
	//},
	//Route{
	//	"NewPost",
	//	[]string{"GET"},
	//	"/author/postseries/{seriesjtitle}/new_post/",
	//	plainMW.ThenFunc(author.NewPostInSeries),
	//},
	//Route{
	//	"EditPostSeries",
	//	[]string{"GET"},
	//	"/author/postseries/{seriesjtitle}/edit/",
	//	plainMW.ThenFunc(author.EditSeries),
	//},
	Route{
		"EditPost",
		[]string{"GET", "POST"},
		"/author/post/{yearadded}/{monthadded}/{dayadded}/{joinedtitle}/edit/",
		authMW.ThenFunc(author.EditPost),
	},

	Route{
		"Static3",
		[]string{"GET"},
		"/static/{s}/{ss}/{sss}/{ssss}/{f}",
		plainMW.ThenFunc(handlers.StaticFiles),
	},

	Route{
		"Static2",
		[]string{"GET"},
		"/static/{s}/{ss}/{sss}/{f}",
		plainMW.ThenFunc(handlers.StaticFiles),
	},
	Route{
		"Static1",
		[]string{"GET"},
		"/static/{s}/{ss}/{f}",
		plainMW.ThenFunc(handlers.StaticFiles),
	},
	Route{
		"Static0",
		[]string{"GET"},
		"/static/{s}/{f}",
		plainMW.ThenFunc(handlers.StaticFiles),
	},
}


func NewRouter() *mux.Router {
	router := mux.NewRouter().StrictSlash(true)
	for _, route := range routeList {
		var handler http.Handler
		handler = route.Handler
		router.
			Methods(route.Methods...).
			Path(route.Pattern).
			Name(route.Name).
			Handler(handler)
	}
	return router
}
