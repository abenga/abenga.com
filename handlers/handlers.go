package handlers

import (
	"html/template"
	"net/http"
	"strconv"
	"strings"
	"fmt"
)

import (
	"github.com/gorilla/context"
	"github.com/gorilla/mux"
)

import (
	"bitbucket.org/abenga/abenga.com/lib/models"
	"bitbucket.org/abenga/abenga.com/lib"
)


const NUM_BASE = 10


func Index(w http.ResponseWriter, r *http.Request) {
	pageContext := context.Get(r, "PageContext").(*lib.PageContext)

	var indexTmpl = template.Must(template.ParseFiles(pageContext.BasePath + "/templates/base.html", pageContext.BasePath + "/templates/index.html"))
	err := indexTmpl.Execute(w, pageContext)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}


func Posts(w http.ResponseWriter, r *http.Request) {
	pageContext := context.Get(r, "PageContext").(*lib.PageContext)

	dbConn, _ := lib.GetDBConn()
	defer dbConn.Conn.Close()

	where := make(map[string]interface{}, 0)
	allPosts, err := models.GetPostMetadata(where, "date_added DESC", "", dbConn)
	if err != nil || len(*allPosts) == 0 {
		http.Error(w, "Could not retrieve post data", http.StatusInternalServerError)
		return
	}
	var postData []map[string]template.HTML
	for _, postModel := range *allPosts{
		p := make(map[string]template.HTML)
		p["Title"] = template.HTML(postModel.Title)
		p["JoinedTitle"] = template.HTML(postModel.JoinedTitle)
		p["AbstractMD"] = template.HTML(postModel.AbstractMD)
		p["AbstractHTML"] = template.HTML(postModel.AbstractHTML)
		p["DateAdded"] = template.HTML(postModel.DateAdded.Format("January 2, 2006"))
		p["YearAdded"] = template.HTML(postModel.DateAdded.Format("2006"))
		p["MonthAdded"] = template.HTML(postModel.DateAdded.Format("1"))
		p["DayAdded"] = template.HTML(postModel.DateAdded.Format("2"))
		p["Tags"] = template.HTML(strings.Join(postModel.Tags, ", "))
		postData = append(postData, p)
	}
	pageContext.Misc["Posts"] = postData

	var postSeriesTmpl = template.Must(template.ParseFiles(pageContext.BasePath + "/templates/base.html", pageContext.BasePath + "/templates/posts.html"))
	if err := postSeriesTmpl.Execute(w, pageContext); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}


func PostSeries(w http.ResponseWriter, r *http.Request) {
	pageContext := context.Get(r, "PageContext").(*lib.PageContext)

	pageVars := mux.Vars(r)

	dbConn, _ := lib.GetDBConn()
	defer dbConn.Conn.Close()

	where := make(map[string]interface{}, 0)
	where["joined_title"] = pageVars["seriesjtitle"]
	allPostSeries, err := models.GetPostSeries(where, "", "", dbConn)
	if err != nil || len(*allPostSeries) != 1 {
		http.Error(w, "Could not retrieve series data", http.StatusInternalServerError)
		return
	}
	postSeries := (*allPostSeries)[0]
	s := make(map[string]template.HTML)
	s["Title"] = template.HTML(postSeries.Title)
	s["JoinedTitle"] = template.HTML(postSeries.JoinedTitle)
	s["AbstractMD"] = template.HTML(postSeries.AbstractMD)
	s["AbstractHTML"] = template.HTML(postSeries.AbstractHTML)
	s["Tags"] = template.HTML(strings.Join(postSeries.Tags, ", "))
	pageContext.Misc["PostSeries"] = s

	postModels, err := postSeries.GetPosts(dbConn)
	if postModels != nil {
		posts := make(map[int64](map[string]template.HTML), 0)
		for _, p := range *postModels {
			post := make(map[string]template.HTML)
			post["PositionInSeries"] = template.HTML(strconv.FormatInt(p.PositionInSeries, NUM_BASE))
			post["Title"] = template.HTML(p.Title)
			post["JoinedTitle"] = template.HTML(p.JoinedTitle)
			post["AbstractHTML"] = template.HTML(p.AbstractHTML)
			post["DateAdded"] = template.HTML(p.DateAdded.Format("January 2, 2006"))
			post["YearAdded"] = template.HTML(p.DateAdded.Format("2006"))
			post["MonthAdded"] = template.HTML(p.DateAdded.Format("1"))
			post["DayAdded"] = template.HTML(p.DateAdded.Format("2"))
			posts[p.PositionInSeries] = post
		}
		pageContext.Misc["Posts"] = posts
	}

	var postSeriesTmpl = template.Must(template.ParseFiles(pageContext.BasePath + "/templates/base.html", pageContext.BasePath + "/templates/postseries.html"))
	if err := postSeriesTmpl.Execute(w, pageContext); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}


func Post(w http.ResponseWriter, r *http.Request) {
	pageCTX := context.Get(r, "PageContext").(*lib.PageContext)

	dbConn, _ := lib.GetDBConn()
	defer dbConn.Conn.Close()

	pageVars := mux.Vars(r)
	yearAdded, _ := strconv.Atoi(pageVars["yearadded"])
	monthAdded, _ := strconv.Atoi(pageVars["monthadded"])
	dayAdded, _ := strconv.Atoi(pageVars["dayadded"])
	joinedTitle := pageVars["joinedtitle"]

	where := map[string]interface{}{"joined_title": joinedTitle, "year_added": yearAdded, "month_added": monthAdded, "day_added": dayAdded}
	posts, err := models.GetPosts(where, "", "", dbConn)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	if len(*posts) != 1 {
		http.Error(w, fmt.Sprintf("Could not retrieve post data. Expected 1 posts got %v.", len(*posts)), http.StatusInternalServerError)
		return
	}
	postModel := (*posts)[0]
	post := make(map[string]template.HTML)
	post["Title"] = template.HTML(postModel.Title)
	post["JoinedTitle"] = template.HTML(postModel.JoinedTitle)
	post["AbstractHTML"] = template.HTML(postModel.AbstractHTML)
	post["BodyHTML"] = template.HTML(postModel.BodyHTML)
	post["DateAdded"] = template.HTML(postModel.DateAdded.Format("January 2, 2006"))
	post["YearAdded"] = template.HTML(postModel.DateAdded.Format("2006"))
	post["MonthAdded"] = template.HTML(postModel.DateAdded.Format("1"))
	post["DayAdded"] = template.HTML(postModel.DateAdded.Format("2"))
	postSeries, _ := postModel.GetPostSeries(dbConn)
	if postSeries != nil {
		post["PositionInSeries"] = template.HTML(strconv.FormatInt(postModel.PositionInSeries, NUM_BASE))
		post["SeriesTitle"] = template.HTML(postModel.Series.Title)
		post["SeriesJoinedTitle"] = template.HTML(postModel.Series.JoinedTitle)
		post["SeriesAbstract"] = template.HTML(postModel.Series.AbstractHTML)
		if postModel.PositionInSeries > 1 && postModel.PositionInSeries < postModel.Series.NumberOfPosts(dbConn) {
			previousPostIndex := postModel.PositionInSeries - 1
			where := map[string]interface{}{"series_id": postSeries.ID, "position_in_series": previousPostIndex}
			postMetadataList, _ := models.GetPostMetadata(where, "", "", dbConn)

			post["PreviousPostPositionInSeries"] = template.HTML(strconv.FormatInt((*(postMetadataList))[0].PositionInSeries, NUM_BASE))
			post["PreviousPostTitle"] = template.HTML((*(postMetadataList))[0].Title)
			post["PreviousPostJoinedTitle"] = template.HTML((*(postMetadataList))[0].JoinedTitle)
			post["PreviousPostYearAdded"] = template.HTML((*(postMetadataList))[0].DateAdded.Format("2006"))
			post["PreviousPostMonthAdded"] = template.HTML((*(postMetadataList))[0].DateAdded.Format("1"))
			post["PreviousPostDayAdded"] = template.HTML((*(postMetadataList))[0].DateAdded.Format("2"))
		}
		if postModel.PositionInSeries < postModel.Series.NumberOfPosts(dbConn) {
			nextPostIndex := postModel.PositionInSeries + 1
			where := map[string]interface{}{"series_id": postSeries.ID, "position_in_series": nextPostIndex}
			postMetadataList, _ := models.GetPostMetadata(where, "", "", dbConn)
			post["NextPostPositionInSeries"] = template.HTML(strconv.FormatInt((*(postMetadataList))[0].PositionInSeries, NUM_BASE))
			post["NextPostTitle"] = template.HTML((*(postMetadataList))[0].Title)
			post["NextPostJoinedTitle"] = template.HTML((*(postMetadataList))[0].JoinedTitle)
			post["NextPostYearAdded"] = template.HTML((*(postMetadataList))[0].DateAdded.Format("2006"))
			post["NextPostMonthAdded"] = template.HTML((*(postMetadataList))[0].DateAdded.Format("1"))
			post["NextPostDayAdded"] = template.HTML((*(postMetadataList))[0].DateAdded.Format("2"))
		}
	}
	pageCTX.Post = post

	var postTmpl = template.Must(template.ParseFiles(pageCTX.BasePath + "/templates/base.html", pageCTX.BasePath + "/templates/post.html"))
	if err := postTmpl.Execute(w, pageCTX); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}


func StaticFiles(w http.ResponseWriter, r *http.Request) {
	// TODO: Verify that path is child of /static/ directory
	pageCTX := context.Get(r, "PageContext").(*lib.PageContext)
	http.ServeFile(w, r, pageCTX.BasePath + r.URL.Path)
}
