package admin

import (
	"encoding/json"
	"html/template"
	"log"
	"net/http"
	"strconv"
	//"strings"
	//"time"
)

import (
	grllContext "github.com/gorilla/context"
	"golang.org/x/net/context"
	//"github.com/gorilla/mux"
	//"github.com/russross/blackfriday"
	"golang.org/x/oauth2"
	//"golang.org/x/oauth2/google"
	//"io/ioutil"
)

import (
	//models "bitbucket.org/abenga/abenga.com/lib/models"
	"bitbucket.org/abenga/abenga.com/lib"
	"golang.org/x/oauth2/google"
	//"io/ioutil"
	//"time"
	//"errors"
	"io/ioutil"
	"bitbucket.org/abenga/abenga.com/lib/models"
	//"time"
	"github.com/gorilla/mux"
	"strings"
	"github.com/russross/blackfriday"
	"time"
	"fmt"
)


//var registerTmpl = template.Must(template.ParseFiles("templates/base.html", "templates/author/register.html"))
//var newPostSeriesTmpl = template.Must(template.ParseFiles("templates/base.html", "templates/author/newpostseries.html"))
//var editPostSeriesTmpl = template.Must(template.ParseFiles("templates/base.html", "templates/author/editpostseries.html"))
//var newPostTmpl = template.Must(template.ParseFiles("templates/base.html", "templates/author/newpost.html"))
//var editPostTmpl = template.Must(template.ParseFiles("templates/base.html", "templates/author/editpost.html"))


const NUM_BASE = 10

// Author home page.
func Home(w http.ResponseWriter, r *http.Request) {
	pageCTX := grllContext.Get(r, "PageContext").(*lib.PageContext)

	dbConn, _ := lib.GetDBConn()
	defer dbConn.Conn.Close()

	where := make(map[string]interface{}, 0)
	where["author_id"] = pageCTX.Author.ID
	orderBy := "DateAdded DESC"
	limit := ""
	postSeriesModels, _ := models.GetPostSeries(where, orderBy, limit, dbConn)
	if postSeriesModels != nil {
		postSeries := make([]map[string]template.HTML, 0)
		for _, s := range *postSeriesModels {
			nPosts := s.NumberOfPosts(dbConn)
			seriesEntry := make(map[string]template.HTML)
			seriesEntry["Title"] = template.HTML(s.Title)
			seriesEntry["JoinedTitle"] = template.HTML(s.JoinedTitle)
			seriesEntry["AbstractHTML"] = template.HTML(s.AbstractHTML)
			seriesEntry["CoverImage"] = template.HTML(s.CoverImagePath)
			seriesEntry["NumberOfPosts"] = template.HTML(strconv.FormatInt(nPosts, NUM_BASE))
			postSeries = append(postSeries, seriesEntry)
		}
		pageCTX.Misc["AuthorPostSeries"] = postSeries
	}

	where = make(map[string]interface{}, 0)
	limitStr := ""
	orderByStr := "date_added"
	postModels, _ := models.GetPostMetadata(where, orderByStr, limitStr, dbConn)
	if postModels != nil {
		posts := make([]map[string]template.HTML, 0)
		for _, p := range *postModels {
			post := make(map[string]template.HTML)
			post["Title"] = template.HTML(p.Title)
			post["JoinedTitle"] = template.HTML(p.JoinedTitle)
			post["PositionInSeries"] = template.HTML(p.PositionInSeries)
			post["AbstractHTML"] = template.HTML(p.AbstractHTML)
			post["DateAdded"] = template.HTML(p.DateAdded.Format("January 2, 2006"))
			post["YearAdded"] = template.HTML(p.DateAdded.Format("2006"))
			post["MonthAdded"] = template.HTML(p.DateAdded.Format("1"))
			post["DayAdded"] = template.HTML(p.DateAdded.Format("2"))
			if p.Series != nil {
				series, _ := p.GetPostSeries(dbConn)
				post["PositionInSeries"] = template.HTML(strconv.FormatInt(p.PositionInSeries, NUM_BASE))
				post["SeriesTitle"] = template.HTML(series.Title)
				post["SeriesJoinedTitle"] = template.HTML(series.JoinedTitle)
			}
			posts = append(posts, post)
		}
		pageCTX.Misc["AuthorPosts"] = posts
	}
	var homeTmpl = template.Must(template.ParseFiles(pageCTX.BasePath + "/templates/base.html", pageCTX.BasePath + "/templates/author/home.html"))
	if err := homeTmpl.Execute(w, pageCTX); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	return
}


func SignIn(w http.ResponseWriter, r *http.Request) {
	var c lib.Configuration
  c.GetConfiguration()
	randomString, err := lib.CreateOauthRandomString(w, r)
	if err != nil {
		http.Error(w, "There was an error signing in.", http.StatusInternalServerError)
		return
	}

	clientID, clientSecret, oauthRedirectURL := lib.GetOauthClientDetails(c, r)
	var (
			googleOauthConfig = &oauth2.Config{
				RedirectURL:    oauthRedirectURL,
				ClientID:     	clientID,
				ClientSecret: 	clientSecret,
				Scopes:       	[]string{"https://www.googleapis.com/auth/userinfo.profile", "https://www.googleapis.com/auth/userinfo.email"},
				Endpoint:     	google.Endpoint,
			}
			oauthStateString = randomString
		)

	url := googleOauthConfig.AuthCodeURL(oauthStateString)
	http.Redirect(w, r, url, http.StatusTemporaryRedirect)
}


func GoogleCallback(w http.ResponseWriter, r *http.Request) {
	//pageContext := grllContext.Get(r, "PageContext").(*lib.PageContext)

	dbConn, _ := lib.GetDBConn()
	defer dbConn.Conn.Close()

  var c lib.Configuration
  c.GetConfiguration()

  var cntxt context.Context

  clientID, clientSecret, oauthRedirectURL := lib.GetOauthClientDetails(c, r)
  var googleOauthConfig = &oauth2.Config{
        RedirectURL:    oauthRedirectURL,
        ClientID:       clientID,
        ClientSecret:   clientSecret,
        Scopes:         []string{"https://www.googleapis.com/auth/userinfo.profile", "https://www.googleapis.com/auth/userinfo.email"},
        Endpoint:       google.Endpoint,
      }

  if _, err := lib.GetSavedOauthRandomString(w, r); err == nil {
		code := r.URL.Query().Get("code")

    token, err := googleOauthConfig.Exchange(cntxt, code)
    if err != nil {
      log.Println("Could not retrieve oauth tokens...")
      http.Error(w, err.Error(), http.StatusInternalServerError)
      return
    }

    client := googleOauthConfig.Client(cntxt, token)
    resp, err := client.Get("https://www.googleapis.com/userinfo/v2/me")
    defer resp.Body.Close()
    if err != nil {
      log.Println("Could not get user details from Google")
      http.Error(w, err.Error(), http.StatusInternalServerError)
      return
    }

    raw, err := ioutil.ReadAll(resp.Body)
    if err != nil {
      log.Println("Could not read response from Google")
      http.Error(w, err.Error(), http.StatusInternalServerError)
      return
    }

    var profile map[string]interface{}
    if err := json.Unmarshal(raw, &profile); err != nil {
      log.Println("Error unmarshalling Google response")
      http.Error(w, err.Error(), http.StatusInternalServerError)
      return
    }

    where := make(map[string]interface{}, 0)
		where["email"] = profile["email"]
		authors, err := models.GetAuthors(where, dbConn)
		if err != nil || len(*authors) != 1 {
			http.Error(w, "Could not retrieve the author's data", http.StatusInternalServerError)
			return
		}

		author := (*authors)[0]

		where = make(map[string]interface{}, 0)
		where["person_id"] = author.ID
		where["ended"] = false
    loginSessions, err := models.GetLoginSessions(where, dbConn)
    tx, _ := dbConn.Conn.Beginx()
    if err == nil {
      for _, loginSession := range *loginSessions {
				loginSession.EndSession(tx)
			}
    }

    sessionID, err := lib.GetSessionID(w, r)
    if err != nil {
      http.Error(w, err.Error(), http.StatusInternalServerError)
      return
    }
    log.Println(sessionID)
    _, err = author.Person.SaveSessionVars(sessionID, tx)
		if err != nil {
			tx.Rollback()
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		tx.Commit()

		store := lib.GetSessionStore()
		session, _ := store.Get(r, lib.SESSION_NAME)
		session.Values["SessionID"] = sessionID
		session.Save(r, w)

		http.Redirect(w, r,"/author/", http.StatusFound)
		return
	}



  http.Error(w, "custom error...", http.StatusInternalServerError)
}


//// Register new author.
//func Register(w http.ResponseWriter, r *http.Request) {
//	pageCTX := context.Get(r, "PageData").(*lib.PageContext)
//
//	if r.Method == "GET" {
//		// var author models.Author
//		u := user.Current(c)
//		if u != nil {
//			author := new(models.Author)
//			author.Email = u.Email
//			pageCTX.Author = author
//			if err := registerTmpl.Execute(w, pageCTX); err != nil {
//				http.Error(w, err.Error(), http.StatusInternalServerError)
//			}
//		} else {
//			url, err := user.LoginURL(c, r.URL.String())
//			if err != nil {
//				http.Error(w, err.Error(), http.StatusInternalServerError)
//				return
//			}
//			w.Header().Set("Location", url)
//			w.WriteHeader(http.StatusFound)
//		}
//	} else if r.Method == "POST" {
//		var author models.Author
//		u := user.Current(c)
//		if u != nil {
//			author.Email = u.Email
//			if firstname := r.PostFormValue("FirstName"); firstname != "" {
//				author.FirstName = firstname
//			}
//			if lastname := r.PostFormValue("LastName"); lastname != "" {
//				author.LastName = lastname
//			}
//			if othernames := r.PostFormValue("OtherNames"); othernames != "" {
//				author.OtherNames = othernames
//			}
//			if biomd := r.PostFormValue("BioMD"); biomd != "" {
//				author.BioMD = biomd
//				author.BioHTML = string(blackfriday.MarkdownBasic([]byte(biomd)))
//			}
//			if _, err := datastore.Put(c, datastore.NewIncompleteKey(c, "Author", nil), &author); err == nil {
//				w.Header().Set("Location", "/author/")
//				w.WriteHeader(http.StatusFound)
//				return
//			} else {
//				http.Error(w, err.Error(), http.StatusInternalServerError)
//				return
//			}
//		}
//	}
//}
//
//
//// Add a new post series.
//func NewPostSeries(w http.ResponseWriter, r *http.Request) {
//	pageCTX := context.Get(r, "PageData").(*lib.PageContext)
//	if r.Method == "GET" {
//		if err := newPostSeriesTmpl.Execute(w, pageCTX); err != nil {
//			http.Error(w, err.Error(), http.StatusInternalServerError)
//		}
//	} else if r.Method == "POST" {
//		var series models.PostSeries
//		if title := r.PostFormValue("Title"); title != "" {
//			series.Title = title
//			series.JoinedTitle = strings.Join(strings.Split(strings.ToLower(title), " "), "-")
//		}
//		if abstractMD := r.PostFormValue("AbstractMD"); abstractMD != "" {
//			series.AbstractMD = abstractMD
//			series.AbstractHTML = string(blackfriday.MarkdownBasic([]byte(abstractMD)))
//		}
//		if tagStr := r.PostFormValue("Tags"); tagStr != "" {
//			tags := strings.Split(tagStr, ",")
//			for i, tag := range tags {
//				tags[i] = strings.Trim(tag, " \t")
//			}
//			series.Tags = tags
//		}
//		series.Author = pageCTX.Author.Key
//		series.DateAdded = time.Now()
//
//		if _, err := datastore.Put(c, datastore.NewIncompleteKey(c, "PostSeries", nil), &series); err == nil {
//			http.Redirect(w, r, "/author/", http.StatusFound)
//			return
//		} else {
//			http.Error(w, err.Error(), http.StatusInternalServerError)
//			return
//		}
//	}
//}
//
//
//// Add a new post.
//func NewPost(w http.ResponseWriter, r *http.Request) {
//	pageCTX := context.Get(r, "PageData").(*lib.PageContext)
//	if r.Method == "GET" {
//		if err := newPostTmpl.Execute(w, pageCTX); err != nil {
//			http.Error(w, err.Error(), http.StatusInternalServerError)
//		}
//	} else if r.Method == "POST" {
//		var post models.Post
//		if title := r.PostFormValue("Title"); title != "" {
//			post.Title = title
//			post.JoinedTitle = strings.Join(strings.Split(strings.ToLower(title), " "), "-")
//		}
//		if abstractMD := r.PostFormValue("AbstractMD"); abstractMD != "" {
//			post.AbstractMD = abstractMD
//			post.AbstractHTML = string(blackfriday.MarkdownBasic([]byte(abstractMD)))
//		}
//		if bodymd := r.PostFormValue("BodyMD"); bodymd != "" {
//			post.BodyMD = bodymd
//			post.BodyHTML = string(blackfriday.MarkdownBasic([]byte(bodymd)))
//		}
//		if tagStr := r.PostFormValue("Tags"); tagStr != "" {
//			tags := strings.Split(tagStr, ",")
//			for i, tag := range tags {
//				tags[i] = strings.Trim(tag, " \t")
//			}
//			post.Tags = tags
//		}
//
//		post.Author = pageCTX.Author.Key
//		post.DateAdded = time.Now()
//		post.YearAdded = time.Now().Year()
//		post.MonthAdded = int(time.Now().Month())
//		post.DayAdded = time.Now().Day()
//
//		if _, err := datastore.Put(c, datastore.NewIncompleteKey(c, "Post", nil), &post); err == nil {
//			http.Redirect(w, r, "/author/", http.StatusFound)
//			return
//		} else {
//			http.Error(w, err.Error(), http.StatusInternalServerError)
//			return
//		}
//	}
//}
//
//
//// Add a new post to series.
//func NewPostInSeries(w http.ResponseWriter, r *http.Request) {
//	pagevars := mux.Vars(r)
//	pageCTX := context.Get(r, "PageData").(*lib.PageContext)
//	pageCTX.Misc["SeriesJTitle"] = pagevars["seriesjtitle"]
//	if r.Method == "GET" {
//		if err := newPostTmpl.Execute(w, pageCTX); err != nil {
//			http.Error(w, err.Error(), http.StatusInternalServerError)
//		}
//	} else if r.Method == "POST" {
//		q := datastore.NewQuery("PostSeries").Filter("JoinedTitle = ", pagevars["seriesjtitle"]).Limit(10)
//		n, err := q.Count(c)
//		if err != nil || n != 1 {
//			http.Error(w, "There was an error fetching the post series details.", http.StatusInternalServerError)
//			return
//		}
//		var postseries = make([]models.PostSeries, 0, 1)
//		keys, err := q.GetAll(c, &postseries)
//		if err != nil {
//			http.Error(w, err.Error(), http.StatusInternalServerError)
//			return
//		}
//		seriesKey := keys[0]
//		var post models.Post
//		post.Series = seriesKey
//		if title := r.PostFormValue("Title"); title != "" {
//			post.Title = title
//			post.JoinedTitle = strings.Join(strings.Split(strings.ToLower(title), " "), "-")
//		}
//		if abstractMD := r.PostFormValue("AbstractMD"); abstractMD != "" {
//			post.AbstractMD = abstractMD
//			post.AbstractHTML = string(blackfriday.MarkdownBasic([]byte(abstractMD)))
//		}
//		if bodyMD := r.PostFormValue("BodyMD"); bodyMD != "" {
//			post.BodyMD = bodyMD
//			post.BodyHTML = string(blackfriday.MarkdownBasic([]byte(bodyMD)))
//		}
//		if tagStr := r.PostFormValue("Tags"); tagStr != "" {
//			tags := strings.Split(tagStr, ",")
//			for i, tag := range tags {
//				tags[i] = strings.Trim(tag, " \t")
//			}
//			post.Tags = tags
//		}
//		q = datastore.NewQuery("Post").Filter("Series = ", seriesKey)
//		if n, err := q.Count(c); err == nil {
//			post.PositionInSeries = n + 1
//		} else {
//			http.Error(w, err.Error(), http.StatusInternalServerError)
//			return
//		}
//
//		post.Author = pageCTX.Author.Key
//		post.DateAdded = time.Now()
//		post.YearAdded = time.Now().Year()
//		post.MonthAdded = int(time.Now().Month())
//		post.DayAdded = time.Now().Day()
//
//		if _, err := datastore.Put(c, datastore.NewIncompleteKey(c, "Post", nil), &post); err == nil {
//			http.Redirect(w, r, "/author/", http.StatusFound)
//			return
//		} else {
//			http.Error(w, err.Error(), http.StatusInternalServerError)
//			return
//		}
//	}
//}
//
//
//// Edit post series
//func EditSeries(w http.ResponseWriter, r *http.Request) {
//	pageVars := mux.Vars(r)
//	pageCTX := context.Get(r, "PageData").(*lib.PageContext)
//	q := datastore.NewQuery("PostSeries").Filter("JoinedTitle = ", pageVars["seriesjtitle"])
//	n, err := q.Count(c)
//	if err != nil || n != 1 {
//		http.Error(w, "There was an error retrieving the post series details.", http.StatusInternalServerError)
//		return
//	}
//
//	t := q.Run(c)
//	var s models.PostSeries
//	k, err := t.Next(&s)
//	if err != nil {
//		http.Error(w, "There was an error retrieving the post series details.", http.StatusInternalServerError)
//		return
//	}
//
//	postSeries := make(map[string]template.HTML)
//
//	postSeries["Title"] = template.HTML(s.Title)
//	postSeries["JoinedTitle"] = template.HTML(s.JoinedTitle)
//	postSeries["AbstractMD"] = template.HTML(s.AbstractMD)
//	postSeries["Tags"] = template.HTML(strings.Join(s.Tags, ", "))
//
//	pageCTX.Misc["PostSeries"] = postSeries
//
//	if r.Method == "GET" {
//		if err := editPostSeriesTmpl.Execute(w, pageCTX); err != nil {
//			http.Error(w, err.Error(), http.StatusInternalServerError)
//		}
//	} else if r.Method == "POST" {
//		if title := r.PostFormValue("Title"); title != "" {
//			if s.Title != title {
//				s.Title = title
//				s.JoinedTitle = strings.Join(strings.Split(strings.ToLower(title), " "), "-")
//			}
//		}
//		if abstractMD := r.PostFormValue("AbstractMD"); abstractMD != "" {
//			if s.AbstractMD != abstractMD {
//				s.AbstractMD = abstractMD
//				s.AbstractHTML = string(blackfriday.MarkdownBasic([]byte(abstractMD)))
//			}
//		}
//		if tagStr := r.PostFormValue("Tags"); tagStr != "" {
//			tags := strings.Split(tagStr, ",")
//			for i, tag := range tags {
//				tags[i] = strings.Trim(tag, " \t")
//				inTags := false
//				for _, tag := range s.Tags {
//					if tags[i] == tag {
//						inTags = true
//					}
//				}
//				if !inTags {
//					s.Tags = append(s.Tags, tag)
//				}
//			}
//		}
//		s.LastEdited = time.Now()
//
//		_, err := datastore.Put(c, k, &s)
//		if err != nil {
//			http.Error(w, err.Error(), http.StatusInternalServerError)
//			return
//		}
//		w.Header().Set("Location", "/author/")
//		w.WriteHeader(http.StatusFound)
//		return
//	}
//}


func EditPost(w http.ResponseWriter, r *http.Request) {
	pageCTX := grllContext.Get(r, "PageContext").(*lib.PageContext)

	dbConn, _ := lib.GetDBConn()
	defer dbConn.Conn.Close()

	pageVars := mux.Vars(r)
	yearAdded, _ := strconv.Atoi(pageVars["yearadded"])
	monthAdded, _ := strconv.Atoi(pageVars["monthadded"])
	dayAdded, _ := strconv.Atoi(pageVars["dayadded"])
	joinedTitle := pageVars["joinedtitle"]

	where := map[string]interface{}{"joined_title": joinedTitle, "year_added": yearAdded, "month_added": monthAdded, "day_added": dayAdded}
	posts, err := models.GetPosts(where, "", "", dbConn)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	if len(*posts) != 1 {
		http.Error(w, fmt.Sprintf("Could not retrieve post data. Expected 1 posts got %v.", len(*posts)), http.StatusInternalServerError)
		return
	}
	postModel := (*posts)[0]
	post := make(map[string]template.HTML)
	post["Title"] = template.HTML(postModel.Title)
	post["JoinedTitle"] = template.HTML(postModel.JoinedTitle)
	post["AbstractHTML"] = template.HTML(postModel.AbstractHTML)
	post["AbstractMD"] = template.HTML(postModel.AbstractMD)
	post["BodyHTML"] = template.HTML(postModel.BodyHTML)
	post["BodyMD"] = template.HTML(postModel.BodyMD)
	post["DateAdded"] = template.HTML(postModel.DateAdded.Format("January 2, 2006"))
	post["YearAdded"] = template.HTML(postModel.DateAdded.Format("2006"))
	post["MonthAdded"] = template.HTML(postModel.DateAdded.Format("1"))
	post["DayAdded"] = template.HTML(postModel.DateAdded.Format("2"))
	post["Tags"] = template.HTML(strings.Join(postModel.Tags,","))

	pageCTX.Post = post

	if r.Method == "GET" {
		var editPostTmpl = template.Must(template.ParseFiles(pageCTX.BasePath + "/templates/base.html", pageCTX.BasePath + "/templates/author/editpost.html"))
		if err := editPostTmpl.Execute(w, pageCTX); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
	} else if r.Method == "POST" {
		changes := make(map[string]interface{}, 0)
		if title := r.PostFormValue("Title"); title != "" {
			if postModel.Title != title {
				postModel.Title = title
				changes["joined_title"] = strings.Join(strings.Split(strings.ToLower(title), " "), "-")
			}
		}
		if abstractMD := r.PostFormValue("AbstractMD"); abstractMD != "" {
			if postModel.AbstractMD != abstractMD {
				changes["abstract_md"] = abstractMD
				changes["abstract_html"] = string(blackfriday.MarkdownCommon([]byte(abstractMD)))
			}
		}
		if bodyMD := r.PostFormValue("BodyMD"); bodyMD != "" {
			if postModel.BodyMD != bodyMD {
				changes["body_md"] = bodyMD
				changes["body_html"] = string(blackfriday.MarkdownCommon([]byte(bodyMD)))
				log.Println(changes["body_html"])
			}
		}
		if tagStr := r.PostFormValue("Tags"); tagStr != "" {
			tags := strings.Split(tagStr, ",")
			changes["tags"] = "{" + lib.ArrayToString(tags, ",") + "}"
		}
		changes["last_edited"] = time.Now()

		log.Println(changes["tags"])

		tx, _ := dbConn.Conn.Beginx()

		err := postModel.Update(tx, changes)
		if err != nil {
			tx.Rollback()
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}

		tx.Commit()

		w.Header().Set("Location", "/author/")
		w.WriteHeader(http.StatusFound)
		return
	}
}
